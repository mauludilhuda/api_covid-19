<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Charts\CovidChart;


class InfoController extends Controller
{
   public function index()
	{
    	$response = Http::get('https://api.kawalcorona.com/indonesia/provinsi');
    	$data = $response->json();
    	return view('index',compact('data'));
	}

     public function indonesia()
    {
        $response = Http::get('https://api.kawalcorona.com/indonesia');
        $indo = $response->json();
        return view('indonesia',compact('indo'));
    }

	 public function global()
	{
    	$response = Http::get('https://api.kawalcorona.com/');

    	$data = $response->json();
    	return view('global',compact('data'));
	}

	 public function chartProvinsi()
    {
        $suspects = collect(Http::get('https://api.kawalcorona.com/indonesia/provinsi')->json());
        
        $labels = $suspects->flatten(1)->pluck('Provinsi');
        $data   = $suspects->flatten(1)->pluck('Kasus_Posi');
        $colors = $labels->map(function($item) {
            return $rand_color = '#' . substr(md5(mt_rand()), 0, 6);
        });

        $chart = new CovidChart;
        $chart->labels($labels);
        $chart->dataset('Kasus Positif', 'pie', $data)->backgroundColor($colors);

        return view('chartProvinsi', ['chart' => $chart,]);
    }

     public function chartglobal()
    {
        $suspects = collect(Http::get('https://api.kawalcorona.com/')->json());
        
        $labels = $suspects->flatten(1)->pluck('Country_Region');
        $data   = $suspects->flatten(1)->pluck('Deaths');
        $colors = $labels->map(function($item) {
            return $rand_color = '#' . substr(md5(mt_rand()), 0, 6);
        });

        $chart = new CovidChart;
        $chart->labels($labels);
        $chart->dataset('Kasus Positif', 'pie', $data)->backgroundColor($colors);

        return view('chartglobal', ['chart' => $chart,]);
    }
}
